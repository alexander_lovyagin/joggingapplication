package com.akvelon.exam.alovyagin.jogging.service;

import com.akvelon.exam.alovyagin.jogging.model.Jogging;
import com.akvelon.exam.alovyagin.jogging.model.User;

import java.security.Principal;
import java.util.List;

public interface JoggingService {
    List<Jogging> findByPrincipal(Principal principal);

    void saveOrUpdate(Jogging jogging);
    void removeByIdAndUser(Long id, User user);

    List<Jogging>  getFullJoggingListForPtintingTest();
}
