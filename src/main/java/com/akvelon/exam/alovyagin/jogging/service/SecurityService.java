package com.akvelon.exam.alovyagin.jogging.service;

public interface SecurityService {
    String findLoggedInUsername();
    void autoLogin(String username, String password);
}
