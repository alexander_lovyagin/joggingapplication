package com.akvelon.exam.alovyagin.jogging.service;

import com.akvelon.exam.alovyagin.jogging.model.User;

public interface UserService {
     void save(User user);
     User findByName(String name);
}
