package com.akvelon.exam.alovyagin.jogging.service;

import com.akvelon.exam.alovyagin.jogging.model.User;
import com.akvelon.exam.alovyagin.jogging.repository.UserRepository;
import com.akvelon.exam.alovyagin.jogging.util.LogFactory;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service for Users management over repository {@link UserRepository}
 *
 */


@Service
@Repository
public class CustomUserService implements UserService {

    private final Logger log = LogFactory.getLogger(this.getClass());

    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    /**
     * Transactional save user mathod
     * @param user
     * @return
     */
    public void save(User user) {
        user.setPasshash(bCryptPasswordEncoder.encode(user.getPassword()));
        Session session = sessionFactory.openSession();

        Transaction tx = session.beginTransaction();
        try {
            session.saveOrUpdate(user);
            tx.commit();
            log.info("User successfully saveOrUpdate.");
        }catch (Exception e){
            tx.rollback();
            log.error(e);
        }
        finally {
            session.close();
        }

        /**
         * variant with UserRepository
         */
//        return userRepository.save(name);

    }




    @Override
    public User findByName(String name) {

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq("name", name));
        criteria.setResultTransformer(Transformers.aliasToBean(User.class));
        User user = (User) criteria.uniqueResult();

        return user;

        /**
         * variant with UserRepository
         */
//        return userRepository.findByName(name);
    }
}
