package com.akvelon.exam.alovyagin.jogging.service;

import com.akvelon.exam.alovyagin.jogging.model.User;
import com.akvelon.exam.alovyagin.jogging.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Repository
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private InMemoryUserDetailsManager inMemoryUserDetailsManager;

    @Autowired
    private UserRepository userRepository;


    /**
     * Load {@link UserDetails} by username
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByName(username);


        if ( !inMemoryUserDetailsManager.userExists(username)) {
            inMemoryUserDetailsManager.createUser(
                    org.springframework.security.core.userdetails.User
                            .withUsername(user.getName())
                            .password(user.getPasshash())
                            .roles("USER").build());
        }

        return inMemoryUserDetailsManager.loadUserByUsername(user.getName());
    }

    /**
     * Using for log out
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    public void unloadUserByUsername(String username) throws UsernameNotFoundException {

        inMemoryUserDetailsManager.deleteUser(username);
    }
}
