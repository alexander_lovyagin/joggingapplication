package com.akvelon.exam.alovyagin.jogging.model;

import com.akvelon.exam.alovyagin.jogging.repository.UserRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Set;
import java.util.SortedSet;

/**
 * this class contains some Json annotations, because it can be advisable for REST applications
 * and i can use it )
 */

@Entity
@Table(name = "user")
@JsonTypeName("user")
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;


    @Column(nullable = false)
    @JsonIgnore
    private String passhash;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    @JsonManagedReference(value = "user-joggings")
    @OrderBy("joggingDate ASC ")
    private SortedSet<Jogging> joggings;


    @Transient
    private String password;

    @Transient
    private String confirmPassword;


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (id == null || obj == null || getClass() != obj.getClass())
            return false;
        User that = (User) obj;
        return id.equals(that.id);
    }
    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

//region getters & setters

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getPasshash() {
        return passhash;
    }

    public User setPasshash(String passhash) {
        this.passhash = passhash;
        return this;
    }

    public SortedSet<Jogging> getJoggings() {
        return joggings;
    }

    public User setJoggings(SortedSet<Jogging> joggings) {
        this.joggings = joggings;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public User setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
        return this;
    }

    //endregion
}
