package com.akvelon.exam.alovyagin.jogging.repository;

import com.akvelon.exam.alovyagin.jogging.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Repository for {@link User}
 */

public interface UserRepository extends JpaRepository<User, Long> {
    User findByName(String username);
}
