package com.akvelon.exam.alovyagin.jogging;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.managed.BasicManagedDataSource;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:hibernate.properties")
@EnableTransactionManagement(proxyTargetClass = true)
public class DataConfig {


    @Value("${dataSource.driverClassName}")
    String driverClassName;
    @Value("${dataSource.url}")
    String url;
    @Value("${dataSource.username}")
    String username;
    @Value("${dataSource.password}")
    String password;


    // call destroy method on destroy bean
    @Bean(destroyMethod = "close")
    public DataSource dataSource(){
        BasicDataSource bean = new BasicDataSource();
//        BasicManagedDataSource bean = new BasicManagedDataSource();
        bean.setDriverClassName(driverClassName);
        bean.setUrl(url);
        bean.setUsername(username);
        bean.setPassword(password);

        return bean;

    }



    @Value("${hibernate.dialect}")
    String dialect;
    @Value("${hibernate.show_sql}")
    String show_sql;
    @Value("${hibernate.hbm2ddl.auto}")
    String hbm2ddl_auto;

    @Bean
    public Properties properties(){
        Properties p = new Properties();

        p.put("hibernate.dialect", dialect);
        p.put("hibernate.show_sql", Boolean.valueOf(show_sql));
        p.put("hibernate.hbm2ddl.auto", hbm2ddl_auto);


        return p;
    }




    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

    /***
     * SessionFactory bean for native Hibernate
     * @return
     */
    @Bean
    @Autowired
    public LocalSessionFactoryBean sessionFactory(
            DataSource dataSource,
            Properties properties) {

        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setPackagesToScan("com.akvelon.exam.alovyagin.jogging.model");
        sessionFactory.setHibernateProperties(properties);

        return sessionFactory;
    }

    @Bean
    public HibernateJpaVendorAdapter jpaVendorAdapter(){
        return new HibernateJpaVendorAdapter();
    }

    @Bean
    @Autowired
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            DataSource dataSource,
            HibernateJpaVendorAdapter jpaVendorAdapter,
            Properties properties
    ){
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSource);
        bean.setPackagesToScan("com.akvelon.exam.alovyagin.jogging.model");
        bean.setJpaVendorAdapter(jpaVendorAdapter);
        bean.setJpaProperties(properties);


        return bean;
    }


    @Bean
    @Autowired
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        JpaDialect jpaDialect = new HibernateJpaDialect();
        txManager.setEntityManagerFactory(entityManagerFactory);
        txManager.setJpaDialect(jpaDialect);
        return txManager;
    }


}
