<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Welcome</title>

    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">

    <h1>${message}</h1>
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/users/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome, ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a>
        </h2>
    </c:if>

    <br/>
    <br/>
    <c:if test="${!empty joggingList}">
        <table class="tg" border="1">
            <tr>
                <th width="60">ID</th>
                <th width="100">Date</th>
                <th width="100">Duration (sec)</th>
                <th width="100">Distance (meters)</th>
                <th width="100">Speed (KmpH)</th>
                <th width="200">Description</th>
                <th width="60">Edit</th>
                <th width="60">Delete</th>
            </tr>
            <c:forEach items="${joggingList}" var="j">
                <tr>
                    <td>${j.id}</td>
                    <td><fmt:formatDate value="${j.joggingDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                    <td>${j.timeBySeconds}</td>
                    <td>${j.distanceByMeters}</td>
                    <td>${j.speedByKmpH}</td>
                    <td>${j.description}</td>
                    <td><a href="<c:url value='/edit/${j.id}'/>">Edit</a></td>
                    <td><a href="<c:url value='/remove/${j.id}'/>">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

    <br/>

    <h2>Add a Jogging</h2>

    <c:url var="addAction" value="/set"/>

    <form:form method="POST"  modelAttribute="jogging"  action="${addAction}" commandName="jogging">
        <table>
            <c:if test="${!empty jogging.id}">
                <tr>
                    <td>
                        <form:label path="id">
                            <spring:message text="ID"/>
                        </form:label>
                    </td>
                    <td>
                        <form:input path="id" readonly="true" size="8" disabled="true"/>
                        <form:hidden path="id"/>

                    </td>
                </tr>
            </c:if>
            <tr>
                <td>
                    <form:label path="joggingDate">
                        <spring:message text="Date"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="joggingDate" id="datepicker"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="timeBySeconds">
                        <spring:message text="Duration (sec)"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="timeBySeconds"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="distanceByMeters">
                        <spring:message text="Distance (meters)"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="distanceByMeters"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="speedByKmpH">
                        <spring:message text="Speed (KmpH)"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="speedByKmpH"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="description">
                        <spring:message text="Description"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="description"/>
                </td>
            </tr>




            <tr>
                <td colspan="2">
                    <c:if test="${!empty jogging.id}">
                        <input type="submit"
                               value="<spring:message text="Edit Jogging"/>"/>
                    </c:if>
                    <c:if test="${empty jogging.id}">
                        <input type="submit"
                               value="<spring:message text="Add Jogging"/>"/>
                    </c:if>
                </td>
            </tr>
        </table>
    </form:form>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>